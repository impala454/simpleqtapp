#include "SimpleQtApp/SimpleQtWindow.h"

SimpleQtWindow::SimpleQtWindow()
{
	setWindowTitle("Simple Qt App!");
	centralWidget = new QWidget(this);
	setCentralWidget(centralWidget);
	
	QVBoxLayout *vLayout = new QVBoxLayout(centralWidget);
	QLabel *lblTest = new QLabel("This is a test");
	vLayout->addWidget(lblTest);
}
