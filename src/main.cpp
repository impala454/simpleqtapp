#include <QApplication>

#include "SimpleQtApp/SimpleQtWindow.h"

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	SimpleQtWindow window;
	window.show();
	return app.exec();
}

