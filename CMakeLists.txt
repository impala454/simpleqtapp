cmake_minimum_required(VERSION 2.8.11)

project(SimpleQtApp)

find_package(Qt5Widgets REQUIRED)

set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)

qt5_wrap_cpp(MOC_H ${PROJECT_SOURCE_DIR}/include/${PROJECT_NAME}/SimpleQtWindow.h)

include_directories(${PROJECT_SOURCE_DIR}/include)

set(SRCS 
	${PROJECT_SOURCE_DIR}/src/main.cpp
	${PROJECT_SOURCE_DIR}/src/SimpleQtWindow.cpp)

add_executable(${PROJECT_NAME} ${SRCS} ${MOC_H})

qt5_use_modules(${PROJECT_NAME} Widgets)

